(define-module (erik licenses)
  #:use-module (guix licenses)
  #:export (SimPL-2.0)
  )


(define license (@@ (guix licenses) license))

  (define-public SimPL-2-0
	(license
	 "Simple Public License 2.0"
	 "https://spdx.org/licenses/SimPL-2.0.html"
	 "https://opensource.org/licenses/simple-2-0-html"))
  

(define-module (botsbuildbots)
  #:use-module (guix packages)
  ;;(guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  ;;(guix build-system trivial)
  #:use-module (guix licenses)
  #:use-module (gnu packages lua)
  )

(define newlicense (@@ (guix licenses) license))

 (define SimPL-2.0
   (newlicense
    "Simple Public License 2.0"
    "https://spdx.org/licenses/SimPL-2.0.html"
    "https://opensource.org/licenses/simple-2-0-html"
    ))


(define-public botsbuildbots
(package
 (name "botsbuildbots")
 (version "pre-alpha-2023-04-14")
 (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/fennel-scripts/botsbuildbots.git")
				  (commit "cb9558b3bf40a07503998734e35ceb4f144799d0")))
            (file-name (git-file-name "botsbuildbots" version))
            (sha256
             (base32
              "0icyjyvfk2qrdwr25vvcfva39skjd18sxhi10dz9h2qxw5cnxxz2"))))
 
 (build-system copy-build-system)
 (arguments
  `(#:install-plan
	'(("botsbuildbots.fnl" "share/botsbuildbots/botsbuildbots"))))
  ;;copy $name.fnl to ~.guix-profile(<-implied)/share/$name/$name(without the .fnl)
 (inputs (list lua fennel))
  (synopsis "home-made buildsystem for the 'fennel' dialect of lisp")
  (description
   (string-append
	"(Defun Botsbuildbots () [Botsbuildbots) ]"
	"A simple buildsystem for fennel/lisp"
	"that is written in fennel/lisp"
	"and can install itself."
	"its only dependencies are *fennel* itself,"
	"*lua* as fennel is written in lua,"
	"and whatever tools are needed/used to peform the build/install"
	"(usually just the GNU coreutils)"))
  (home-page "https://www.gnu.org/software/hello/")
  ;; SPDX-License-Identifier: SimPL-2.0
  
  (license SimPL-2.0)))
botsbuildbots

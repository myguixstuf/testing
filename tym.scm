(define-module (tym)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)

  #:use-module (gnu packages gtk)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pkg-config)
  
  #:use-module (gnu packages lua)
  #:use-module (guix licenses))





(define-public tym
  (package
   (name "tym")
   (version "3.4.1")
;;  (source
;;   (origin
;;   (method url-fetch)
;;   ;;https://github.com/endaaman/tym/releases/download/3.4.1/tym-3.4.1.tar.gz
;;   (uri (string-append "https://github.com/endaaman/tym/releases/download/" version "/" name "-" version ".tar.gz"))
;;   (sha256 (base32 "16w6l9k2pp4igahjd29jqabddnbksxy86smw9r04dwj5ss13q46d"))))

   ;;(source (local-file "/home/erik/guixStuff/tym/tym-3.4.1" #:recursive? #t))
;;59a20f0a55e08865bc066f5f5b21824eb2afd0b6
   (source
	(origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/erikLundstedt/tym.git")
		   (commit "59a20f0a55e08865bc066f5f5b21824eb2afd0b6")));;<-change this 
     (file-name (git-file-name "tym" version))
     (sha256
      (base32
       "0fd4yvrcrp3k16k4yi12ljjy9qs6cgj9gw55y18jn6nsh4qp99qb"))));;<-change this

   
   (build-system gnu-build-system)
   (native-inputs (list pkg-config perl m4 autoconf automake))
   (inputs (list gtk+ vte lua pcre2))
   (synopsis "Lua-configurable terminal emulator.")
   (description "a lua-configurable terminal emulator based on VTE")
   (home-page "https://github.com/endaaman/tym")
   (license expat);;defined on github as MIT
   ))

tym

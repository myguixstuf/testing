export SYSTEMD_EDITOR=nvim
alias xdotoolhelper='xdotoolhelper-1.0.4'

source /home/erik/.config/broot/launcher/bash/br

# Automatically added by the Guix install script.
if [ -n "$GUIX_ENVIRONMENT" ]; then
    if [[ $PS1 =~ (.*)"\\$" ]]; then
        PS1="${BASH_REMATCH[1]} [env]\\\$ "
    fi
fi

